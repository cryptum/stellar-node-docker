#!/bin/bash

make build
sleep 5s

docker run --rm -it \
	-p "8000:8000" \
	-v "${VOLUME_DIR}/stellar-data/horizon:/opt/stellar" \
	--name stellar stellar/quickstart --"${NETWORK}"

sleep 5s

docker run -d \
	-p "8000:8000" \
	-v "${VOLUME_DIR}/stellar-data/horizon:/opt/stellar" \
	--name stellar stellar/quickstart --"${NETWORK}"
